/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch4x bare metal application project
 * 		File: - "main.c"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *     - Change checkHW, instead of say 0 or 1, said is ready, done ...
 *     - Add sw implementation, measure time.
 *
 * CHANGELOG:
 *     [24-10-2018]: Hello world
 *     [13-11-2018]: Repo changes.
 *
 *---------------------------------------------------------------------------*/
#include "main.h"

void checkHW()
{
	xil_printf("- Hw module ready: %u\r\n",XI_stretch4x_hw_IsReady(&module_hw));
	xil_printf("- Hw module idle: %u \r\n", XI_stretch4x_hw_IsIdle(&module_hw));
	xil_printf("- Hw module done: %u \r\n", XI_stretch4x_hw_IsDone(&module_hw));

	xil_printf("- DMA is busy direction dma to device: %u\r\n",
			XAxiDma_Busy(&AxiDma, XAXIDMA_DMA_TO_DEVICE));
	xil_printf("- DMA is busy direction device to dma: %u\r\n",
			XAxiDma_Busy(&AxiDma, XAXIDMA_DEVICE_TO_DMA));
}


int devicesReset(u16 DeviceIdDMA, u16 DeviceIdModule)
{
	xil_printf("-------------------------\r\n");
	xil_printf("Entering devicesReset ...\r\n");

	XAxiDma_Config *CfgPtrDMA;

	int Status = XST_SUCCESS;

	xil_printf("- DMA lookup table configuration ... ");
	CfgPtrDMA = XAxiDma_LookupConfig(DeviceIdDMA);
	if (!CfgPtrDMA) {
		return XST_FAILURE;
	}
	xil_printf("OK \r\n");

	xil_printf("- Initializes the DMA engine ... ");
	Status = XAxiDma_CfgInitialize(&AxiDma, CfgPtrDMA);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	xil_printf("OK \r\n");

   XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,XAXIDMA_DEVICE_TO_DMA);
   XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,XAXIDMA_DMA_TO_DEVICE);

	xil_printf("- Stretch4x hw module initialization ... ");
	Status = XI_stretch4x_hw_Initialize(&module_hw, DeviceIdModule);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	xil_printf("OK \r\n");

	xil_printf("Exiting devicesReset ... \r\n");

	return Status;
}


int DMATransfer(u16 DeviceID)
{
	xil_printf("--------------------------------\r\n");
	xil_printf("Entering DMATransfer ... \r\n");

	int Status = XST_SUCCESS;

	u8 value;

	u8 *TxBufferPtr __attribute__((aligned(8)));
	TxBufferPtr = (u8*)TX_BUFFER_BASE;

   	xil_printf("Taking input.txt values ... \r\n");
   	xil_printf("Pointer address -> %p \r\n", input_data);

	for(int i = 0; i < (NUMBER_PIXELS_INPUT);  i++)
	{
		value = *input_data;
		TxBufferPtr[i] = value;
		input_data++;

		if ((i<10) | (i>(NUMBER_PIXELS_INPUT-10)))  {
			xil_printf("Sent, i: %u, value: %u \r\n", i, value);
		}

	}
	xil_printf("OK \r\n");

	Xil_DCacheFlushRange((UINTPTR)TxBufferPtr, NUMBER_PIXELS_INPUT);

	xil_printf("Checking HW before simple transfer ... \r\n");
	checkHW();
	xil_printf("OK \r\n");

	xil_printf("Stretch4x start ... ");
	XI_stretch4x_hw_Start(&module_hw);
	xil_printf("OK \r\n");

	Xil_DCacheFlushRange((UINTPTR)TxBufferPtr, NUMBER_PIXELS_INPUT);

	xil_printf("DMA Simple transfer ... ");
	Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) TxBufferPtr,
			NUMBER_PIXELS_INPUT, XAXIDMA_DMA_TO_DEVICE);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	xil_printf("OK \r\n");

	xil_printf("Checking HW after simple transfer ... \r\n");
	checkHW();
	xil_printf("OK \r\n");

	xil_printf("Exiting DMATransfer ... \r\n");

   	return Status;
}



int DMAReceive(u16 DeviceID)
{
   	xil_printf("--------------------------------\r\n");
   	xil_printf("Entering DMAReceive function \r\n");

	int Status = XST_SUCCESS;

	u8 *RxBufferPtr __attribute__((aligned(8)));
	RxBufferPtr = (u8*)RX_BUFFER_BASE;

	u8 value;

	Xil_DCacheFlushRange((UINTPTR)RxBufferPtr, NUMBER_PIXELS_OUTPUT);

	xil_printf("Checking HW before receive transfer ... \r\n");
	checkHW();
	xil_printf("OK \r\n");

   	xil_printf("DMA take data ... ");
	Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) RxBufferPtr,
			NUMBER_PIXELS_OUTPUT, XAXIDMA_DEVICE_TO_DMA);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	xil_printf("OK \r\n");

   	while ((XAxiDma_Busy(&AxiDma,XAXIDMA_DEVICE_TO_DMA))) {
   		// wait
   	}
	xil_printf("Transfer Device to DMA done !\r\n");

	xil_printf("Checking HW after receive transfer ... \r\n");
	checkHW();
	xil_printf("OK \r\n");

	xil_printf("Checking data received, first and last 10 values ... ");
   	for (int i=0; i<NUMBER_PIXELS_OUTPUT; i++) {

		value = RxBufferPtr[i];

		if ((i<10) | (i>(NUMBER_PIXELS_OUTPUT-10))) {
			xil_printf("Received, i: %u, value: %u \r\n", i, value);
		}

	}
   	xil_printf("OK \r\n");

   	return Status;
}


int main()
{
    xil_printf("\r\n------------------------- \r\n");
   	xil_printf("Entering main ... \r\n");

    int Status;

   	/*------------- Run the DMAReset function to reset the DMA --------------*/
   	Status = devicesReset(DMA_DEV_ID, STRETCH4X_HW_ID);

   	if (Status != XST_SUCCESS) {
   		xil_printf("devicesReset Failed\r\n");
  		return XST_FAILURE;
   	}

   	/*---------------------- Send data to module ----------------------------*/
   	Status = DMATransfer(DMA_DEV_ID);

   	if (Status != XST_SUCCESS) {
		xil_printf("DMATrasnfer Failed\r\n");
		return XST_FAILURE;
   	}

   	/*------------------------ Receive data ---------------------------------*/
   	Status = DMAReceive(DMA_DEV_ID);

   	if (Status != XST_SUCCESS) {
		xil_printf("DMATrasnfer Failed\r\n");
		return XST_FAILURE;
   	}

   	xil_printf("Successfully ran dma_test Example\r\n");
   	xil_printf("--- Exiting main() --- \r\n");

    xil_printf("Checking HW at the end of main ... \r\n");
	checkHW();
	xil_printf("OK \r\n");

    cleanup_platform();

   	return XST_SUCCESS;
}
